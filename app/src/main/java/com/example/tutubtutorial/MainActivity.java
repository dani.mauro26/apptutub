package com.example.tutubtutorial;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    Intent backroundServiceIntent;
    Button PlayPauseBtn, repetirBtn;
    ImageView imageView;
    int posicion = 0;
    boolean repetir = false;
    MediaPlayer mediaPlayerVector[] = new MediaPlayer[4];

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        backroundServiceIntent = new Intent(this, BackroundSound.class);
        PlayPauseBtn = findViewById(R.id.playBtn);
        repetirBtn = findViewById(R.id.repetirBtn);
        imageView = findViewById(R.id.portada_iv);
        ActualizarCanciones();
    }

    public void PlayPause(View view) {
        if (mediaPlayerVector[posicion].isPlaying()) {
            mediaPlayerVector[posicion].pause();
            PlayPauseBtn.setBackgroundResource(R.drawable.reproducir);
            Toast.makeText(this, "Pausa", Toast.LENGTH_SHORT).show();
        } else {
            mediaPlayerVector[posicion].start();
            PlayPauseBtn.setBackgroundResource(R.drawable.pausa);
            Toast.makeText(this, "Play", Toast.LENGTH_SHORT).show();
        }
    }

    public void Stop(View view) {
        if (mediaPlayerVector[posicion] != null) {
            mediaPlayerVector[posicion].stop();
            ActualizarCanciones();
            posicion = 0;
            PlayPauseBtn.setBackgroundResource(R.drawable.reproducir);
            imageView.setImageResource(R.drawable.portada1);
            Toast.makeText(this, "Stop", Toast.LENGTH_SHORT).show();
        }
    }


    public void Repetir(View view) {
        if (repetir == true) {
            repetirBtn.setBackgroundResource(R.drawable.no_repetir);
            Toast.makeText(this, "No Repetir", Toast.LENGTH_SHORT).show();
            mediaPlayerVector[posicion].setLooping(false);
            repetir = false;
        } else {
            repetirBtn.setBackgroundResource(R.drawable.repetir);
            Toast.makeText(this, "Repetir", Toast.LENGTH_SHORT).show();
            mediaPlayerVector[posicion].setLooping(true);
            repetir = true;
        }

    }

    public void Siguiente(View view) {
        if (posicion < mediaPlayerVector.length - 1) {
            if (mediaPlayerVector[posicion].isPlaying()) {
                mediaPlayerVector[posicion].stop();
                posicion++;
                mediaPlayerVector[posicion].start();
                ActualizarPortada(posicion);
            } else {
                posicion++;
                ActualizarPortada(posicion);
            }
        } else {
            Toast.makeText(this, "No hay mas canciones", Toast.LENGTH_SHORT).show();
        }
    }

    public void Anterior(View view) {

        if (posicion >= 1){

            if (mediaPlayerVector[posicion].isPlaying()){
                mediaPlayerVector[posicion].stop();
                ActualizarCanciones();
                posicion--;
                ActualizarPortada(posicion);
                mediaPlayerVector[posicion].start();

            } else{
                posicion--;
                ActualizarPortada(posicion);
            }
        } else {
            Toast.makeText(this, "No hay mas canciones", Toast.LENGTH_SHORT).show();
        }
    }

        public void ActualizarCanciones(){
            mediaPlayerVector[0] = MediaPlayer.create(this, R.raw.race);
            mediaPlayerVector[1] = MediaPlayer.create(this, R.raw.sound);
            mediaPlayerVector[2] = MediaPlayer.create(this, R.raw.tea);
            mediaPlayerVector[3] = MediaPlayer.create(this, R.raw.song2);
        }

        public void ActualizarPortada(int posicion){
            switch (posicion) {
                case 0:
                    imageView.setImageResource(R.drawable.portada1);
                    break;
                case 1:
                    imageView.setImageResource(R.drawable.portada2);
                    break;
                case 2:
                    imageView.setImageResource(R.drawable.portada3);
                    break;
                case 3:
                    imageView.setImageResource(R.drawable.portada4);
                    break;
            }
        }

        @Override
        public void onClick (View v){

        }
    }
